import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { FC, useCallback, useEffect, useRef, useState } from "react";
import { Text, SafeAreaView, TouchableOpacity, View, FlatList } from "react-native";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { Card, CardContainer, CardText, Container, DeleteBtn, DeleteBtnTxt, Input, NextBtn } from "../../components/common-components.styled";
import { RootStackParams } from "../../models/root-stack-params.model";
import { addOperation, removeOperation, updateOperation } from "../../redux/slices/crud.slice";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";

const RdxExample: FC = () => {
    const navigation = useNavigation<NativeStackNavigationProp<RootStackParams>>();
    const crudList = useAppSelector(state => state.crud.customList);
    const dispatch = useAppDispatch();
    const [input, setInput] = useState('');

    const onAdd = useCallback(() => {
        if (input!=='') {
            dispatch(addOperation(input));
        }
    }, [input])

    return <>
    <SafeAreaView style={{flex: 1}}>
        <Container>
            <Text>RdxExample page</Text>
            {/* {JSON.stringify(todos)} */}
            <NextBtn onPress={() => navigation.navigate('QueryExample')}>
                    <Text>Next</Text>
            </NextBtn>
            <View>
                <Input onChangeText={txt => setInput(txt)} />
                <TouchableOpacity onPress={onAdd}>
                    <Text>Add</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                data={crudList} 
                renderItem={ ({ item }) => <CardContainer>
                        <Card>
                            <BouncyCheckbox
                                size={25}
                                fillColor="red"
                                unfillColor="#FFFFFF"
                                text={item.text}
                                iconStyle={{ borderColor: "red" }}
                                isChecked={item.done}
                                onPress={(isChecked: boolean) => {
                                        let obj = {...item};
                                        obj.done = isChecked;
                                        dispatch(updateOperation(obj))
                                    }
                                }
                            />
                        </Card> 
                        <DeleteBtn onPress={() => dispatch(removeOperation(item.id))}>
                            <DeleteBtnTxt>X</DeleteBtnTxt>
                        </DeleteBtn>
                    </CardContainer>}
                keyExtractor={(item,idx) => idx.toString()}
            />
        </Container>
    </SafeAreaView>
    </>
}

// RdxExample.navigationOptions = {
//     headerTitle: 'hello',
// }

export default RdxExample;



