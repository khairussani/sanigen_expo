import styled from "styled-components/native";

export const Container = styled.View`
    background-color: papayawhip;
    flex: 1;
    align-items: center;
    justify-content: center
`;

export const NextBtn = styled.TouchableOpacity`
    min-width: 50%;
    background-color: yellow;
    height: 40px;
    borderRadius: 20px;
    align-items: center;
    justify-content: center
`;

export const Input = styled.TextInput`
    background-color: white;
    width: 200px;
    height: 50px;
`;

export const CardContainer = styled.View`
    flex-direction: row;
`;

export const Card = styled.View`
    border-radius: 20px;
    width: 200px;
    background-color: white;
    margin-top: 8px;
    margin-bottom: 8px;
    flex-direction: row;
`;

export const Card2 = styled.View`
    border-radius: 20px;
    width: 200px;
    background-color: white;
    margin-top: 8px;
    margin-bottom: 8px;
    flex-direction: column;
`;

export const CardText = styled.Text`
    text-align: center;
    flex: 1;
    height: 40px;
    line-height: 40px;
`;

export const DeleteBtn = styled.TouchableOpacity`
    width: 30px;
    height: 30px;
    border-radius: 15px;
    background-color: white;
    align-self: center;
    margin-left: 8px;
`;

export const DeleteBtnTxt = styled.Text`
    text-align: center;
    flex: 1;
    height: 30px;
    line-height: 30px;
`;

export const CheckBoxContainer = styled.Pressable`
    width: 64px;
    height: 64px;
`;