// data structure of json result from api
export type Staff = {
    id: number;
    Staff: string;
    Done: boolean;
}

export type StaffAddType = {
    Staff: string;
    Done: boolean;
}