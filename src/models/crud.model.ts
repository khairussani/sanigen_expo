export type CrudType = {
    id: number,
    done: boolean,
    text: string,
}

export interface CrudListState {
    customList: CrudType[],
}