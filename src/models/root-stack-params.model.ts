export type RootStackParams = {
    RdxExample: undefined;
    QueryExample: undefined;
    UserList: {
      userId: string,
      amount: number
    },
  }