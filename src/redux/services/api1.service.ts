import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Staff, StaffAddType } from '../../models/api1.model';

const url = 'https://retoolapi.dev/3VWKsp/data';

export const todoApi = createApi({
    reducerPath: 'todoApi',
    baseQuery: fetchBaseQuery({ baseUrl: url }),
    tagTypes: ['Staff'],
    endpoints: (builder) => ({
        // <ResultType, QueryArgType> --> format
        getAll: builder.query<Staff[], void>({
            query: () => '/',
            providesTags: ['Staff']
        }),
        getOne: builder.query<Staff, string>({
            query: id => `/${id}`,
            providesTags: ['Staff']
        }),
        addTask: builder.mutation<void, StaffAddType>({
            query: contact => ({
                url: '/',
                method: 'POST',
                body: contact
            }),
            invalidatesTags: ['Staff']
        }),
        updateTask: builder.mutation<void, Staff>({
            query: ({id, ...rest}) => ({
                url: `/${id}`,
                method: 'PUT',
                body: rest
            }),
            invalidatesTags: ['Staff']
        }),
        removeTask: builder.mutation<void, number>({
            query: id => ({
                url: `/${id}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['Staff']
        }),
    })
});

/* query name (GET) is auto generated in format of  'useXXXXXQuery' while
 mutation name (POST,PUT,DELETE) is in format of 'useXXXXXMutation'
*/
export const { 
    useGetAllQuery,
    useGetOneQuery,
    useAddTaskMutation,
    useUpdateTaskMutation,
    useRemoveTaskMutation,
} = todoApi;
