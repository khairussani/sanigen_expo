import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { CrudListState, CrudType } from "../../models/crud.model"

const initialState: CrudListState = {
    customList: [],
}

export const crudSlice = createSlice({
    name: 'crud',
    // `createSlice` will infer the state type from the `initialState` argument
    initialState,
    reducers: {
        // Use the PayloadAction type to declare the contents of `action.payload`
        addOperation: (state, action: PayloadAction<string>) => {
            state.customList = [
                ...state.customList,
                {
                    id: state.customList.length,
                    done: false,
                    text: action.payload
                }
            ]
        },
        removeOperation: (state, action: PayloadAction<number>) => {
            state.customList = state.customList.filter( ({ id }) => id !== action.payload )
        },
        updateOperation: (state, action: PayloadAction<CrudType>) => {
            const index = state.customList.findIndex(a => a.id===action.payload.id);
            state.customList[index].done = action.payload.done;            
        }
    },
})

export const { addOperation, removeOperation, updateOperation } = crudSlice.actions;

export default crudSlice.reducer;
