import { combineReducers, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { setupListeners } from '@reduxjs/toolkit/query/react';
import crudSlice from "./slices/crud.slice";
import { todoApi } from "./services/api1.service";

const rootReducer = combineReducers({
    // add the generated reducer as a specific top-level slice
    [todoApi.reducerPath]: todoApi.reducer,
    crud: crudSlice,
});

export type RootState = ReturnType<typeof rootReducer>;

const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(todoApi.middleware)
});

export default store;

export type AppDispatch = typeof store.dispatch;