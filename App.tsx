import { StatusBar, useColorScheme } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import RdxExample from './src/pages/rdx-example';
import QueryExample from './src/pages/query-example';
import { RootStackParams } from './src/models/root-stack-params.model';

export default function App() {
  const isDarkMode = useColorScheme() === 'dark';
  const Stack = createNativeStackNavigator<RootStackParams>();
  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar barStyle={isDarkMode? 'light-content' : 'dark-content'} />
        <Stack.Navigator >
          <Stack.Screen name='RdxExample' component={RdxExample} />
          <Stack.Screen name='QueryExample' component={QueryExample} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
    
  );
}

